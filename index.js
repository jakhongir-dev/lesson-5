// Task 1
const vowelCounter = function (str) {
  const vowels = ["a", "e", "i", "u", "o"];
  const count = str
    .toLowerCase()
    .split(" ")
    .map((el) => el.split(""))
    .flat()
    .filter((el) => vowels.includes(el)).length;
  console.log(count);
};
vowelCounter("A quick brown firefox");

// Task 2
const daysLeft = function () {
  const today = new Date();
  const year = today.getFullYear();
  const msPerDay = 1000 * 60 * 60 * 24;
  const janFirst = new Date(year + 1, 0, 01);
  const daysLeft = Math.floor((janFirst - today) / msPerDay);
  console.log(`${daysLeft} days left until New Year.`);
};
daysLeft();

// Task 3
const symbolFinder = function (arr) {
  const symbols = arr.filter((el) => {
    const lowerCase = el.charCodeAt() >= 97 && el.charCodeAt() <= 122;
    const upperCase = el.charCodeAt() >= 65 && el.charCodeAt() <= 90;
    return !(lowerCase || upperCase);
  });
  console.log(symbols);
};

symbolFinder(["a", "&", "g", ")", "@", "g", "#", "+", "w", "~", "$", "w"]);

// Task 4
const intersection = function (arr, arr2) {
  const intsec = arr.filter((el) => arr2.includes(el));
  console.log(intsec);
};
intersection([7, 3, "w", 21, "l"], [6, 3, 7, 10, "s"]);

// Task 5
setTimeout(function () {
  alert("Hello, how are you? I'm under the water. Please help me!");
}, 4000);

// Task 6
const sumNat = function (arr) {
  for (const el of arr) {
  }
  const sum = arr
    .filter((el) => typeof el == "number" && el > 0)
    .reduce((acc, cur) => (acc += cur), 0);
  console.log(sum);
};
sumNat([2, "yes", 4, 5, -5, 7, "lol", 6, -5, 2]);

// Task 7
const findChar = function (str, char) {
  const specsCount = str
    .split(" ")
    .flatMap((el) => el.split(""))
    .filter((el) => el == char).length;
  console.log(specsCount);
};
findChar("there was a message which had sent by Donald Trump", "a");
